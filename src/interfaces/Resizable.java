package interfaces;

public interface Resizable {
    public double resize(int percent);

}
